FROM registry.gitlab.com/gitlab-org/security-products/analyzers/secrets:latest
RUN apk add --no-cache bash
RUN apk add --no-cache curl
RUN mv /analyzer /old-analyzer
COPY analyzer /
RUN chmod +x /analyzer
